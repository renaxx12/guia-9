from fighter import Fighter

class Wizard(Fighter):
	def __init__(self):
	    self._type = "Mago"
	    self._spell = False
	    self._vulnerable = False
	    self._damage_points = 0
	#decimos condiciones para ver si el mago es vulnerable o no
	def is_vulnerable(self):
	    if self.spell is False:
	        self.vulnerable = True
	    elif self.spell is True:
	        self.vulnerable = False
	    return self.vulnerable
	#cambiamos la variable para controlar el estado del hechizo
	def prepare_spell(self):
	    print("el mago prepara un hechizo")
	    self.spell = True
	
	def damage_points(self):
	    if self.spell:
	        self.damage = 12
	    else:
	        self.damage = 3
	    print(self.damage)
	
	@property
	def spell(self):
	    return self._spell
	
	@spell.setter
	def spell(self, value):
	    self._spell = value
	
	@property
	def damage(self):
	    return self._damage
	
	@damage.setter
	def damage(self, value):
	    self._damage = value
	
	@property
	def vulnerable(self):
	    return self._vulnerable
	    
	@vulnerable.setter
	def vulnerable(self, value):
	    self._vulnerable = value
	
	@property
	def type(self):
	    return self._type
	
	
	
