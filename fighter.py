from abc import abstractmethod
from abc import ABCMeta
from abc import ABC

"""
esta clase sirve como estructura para crear los 2 tipos de luchadores que existen
"""
class Fighter(metaclass=ABCMeta):
	@abstractmethod
	def is_vulnerable(self):
	    pass
	
	@abstractmethod
	def damage_points(self):
	    pass

