from fighter import Fighter

class Warrior(Fighter):
	def __init__(self):
	    self._type = "Guerrero"
	    self._damage_points = 0
	
	def is_vulnerable(self):
	    return False
	#para saber cuanto daño hace, hay que darle el estado de
	#vulnerabilidad del contrincante
	def damage_points(self, vulnerable):
	    if vulnerable:
	        self.damage = 10
	    else:
	        self.damage = 6
	    print(self.damage)
	
	@property
	def damage(self):
	    return self._damage
	
	@damage.setter
	def damage(self, value):
	    self._damage = value 
	
	@property
	def type(self):
	    return self._type
	


