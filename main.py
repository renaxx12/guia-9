from fighter import Fighter
from magos import Wizard
from warrior import Warrior

"""
no se utilizó la sobrecarga ya que creé 2 clases distintas para cada tipo de guerrero,
por eso solamente utilicé la clase abstracta Fighter de esqueleto para crear ambas clases
"""
warrior = Warrior() 
wizard = Wizard()

#para mostrar el tipo de guerrero que son se hace lo siguiente
print(warrior.type)

#para saber si es vulnerable
print(warrior.is_vulnerable())

#vemos si el mago es vulnerable al no haber preparado un hechizo
print(wizard.is_vulnerable())

#ahora hacemos que el mago haga un hechizo
wizard.prepare_spell()

#ahora que preparó el hechizo volvemos a ver si es vulnerable
print(wizard.is_vulnerable())
#vemos cuanto daño genera el mago
wizard.damage_points()

#vemos los puntos de daño del guerrero
warrior.damage_points(wizard.is_vulnerable())

